# Spacebar Media Test

## Design and build a microservice to implement the following game:
 The game consist in throwing two dices. At the beginning of the game each dice will show number 3 by default (total sum = 6).   
 A player can choose betting on either **higher** number, **lower** number or **same** number.   
 Then the player roll dices and compare outcomes.   
   
 Next bet will be based on the last outcome.   
(For simplicity, wager amount will always be 1p and, in case of winning the amount will always be 10p.)  
 

### Add an endpoint that provides the following information for the last 100 bets:
 - BetId  
 - PlayerID  
 - PreviousOutcome  
 - CurrentOutcome  
 - Date  
 
 
### Build an app the will simulate different players playing at the same time. A total of 1000000 bets should be performed in the minimum time possible and the results saved in a report. (optional)
 